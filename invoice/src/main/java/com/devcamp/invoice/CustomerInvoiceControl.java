package com.devcamp.invoice;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomerInvoiceControl {
    @CrossOrigin
    @GetMapping("/invoices")
    public ArrayList<Invoice> listInvoices() {
        Customer customer1 = new Customer(1, "Giang", 10);
        Customer customer2 = new Customer(2, "Linh", 20);
        Customer customer3 = new Customer(3, "Ninh", 30);

        System.out.println(customer1.toString());
        System.out.println(customer2.toString());
        System.out.println(customer3.toString());

        Invoice  invoice1 = new Invoice(101, customer1, 10000 );
        Invoice  invoice2 = new Invoice(102, customer2, 20000 );
        Invoice  invoice3 = new Invoice(103, customer3, 30000 );

        System.out.println(invoice1.toString());
        System.out.println(invoice2.toString());
        System.out.println(invoice3.toString());

        ArrayList<Invoice> invoices = new ArrayList<>();

        invoices.add(invoice1);
        invoices.add(invoice2);
        invoices.add(invoice3);

        return invoices;
    }

}
